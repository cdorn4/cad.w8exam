//Chris Dorn
//C++ Exam 2 Practical 

#include <iostream> 
#include <conio.h> 
#include <string> 
#include <fstream> 

using namespace std;

float FindAverage(float* pNumbers, const int size)
{
	float total = 0; 
	float average = 0;
	for (int i = 0; i < size;i++)
	{
		total = pNumbers[i] + total;
	}
	average = total / size;
	return average; 
}

float Max(float* pNumbers, const int size)
{
	int temp = pNumbers[0];
	for (int i = 0; i < size; i++) {
		if (temp < pNumbers[i]) {
			temp = pNumbers[i];
		}
	}
	return temp;
}

float Min(float* pNumbers, const int size)
{
	int temp = pNumbers[0];
	for (int i = 0; i < size; i++) {
		if (temp > pNumbers[i]) {
			temp = pNumbers[i];
		}
	}
	return temp;
}


int main()
{
	char save = 'n';

	
	const int size = 5;
	float Numbers[size];

	cout << "Please enter 5 numbers" << "\n";

	for (int i = 0; i < size; i++)
	{
		cin >> Numbers[i];
	}
	
	cout << "You Entered:";
	for (int i = 0; i < size; i++)
	{

		cout << Numbers[i] << ", ";
	}

	cout << "\n" << "The minimum number is " << Min(Numbers, size) << "\n"
	 << "The Maximum number is " << Max(Numbers, size) << "\n"
	 << "The average of your numbers is " << FindAverage(Numbers, size) << "\n"
	 << "The reverse of your numbers is ";
	for (int i = size-1; i >=0 ; i--) 
	{
		cout << Numbers[i] << ", ";
	}


	cout << "\n" << "Would you like to save output to file? (y/n): ";
	cin >> save;

	if (save == 'y' || save == 'Y')
	{
		string filepath = "Exam2.txt";
		ofstream ofs(filepath);
		ofs << "The minimum number is " << Min(Numbers, size) << "\n"
			<< "The Maximum number is " << Max(Numbers, size) << "\n"
			<< "The average of your numbers is " << FindAverage(Numbers, size) << "\n";
		ofs.close();
		cout << "\n" << "Exam 2 has been saved to Exam2.txt";
	}

	cout << "\n" << "Press any key to exit...";

	(void)_getch();
	return 0;
}